import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string='Angular Crud';

  employees =[
    {'name': 'jose', position:'manager'},
    {'name': 'jose', position:'manager'},
    {'name': 'jose', position:'manager'},
    {'name': 'jose', position:'manager'}
  ];

  model:any = {};
  addEmployee():void{

  }

  deleteEmployee():void{

  }
  editEmployee():void{

  }
  updateEmployee():void{
    
      }
}
